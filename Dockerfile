FROM ubuntu:16.04 as base
LABEL name=base
# update dpkg repositories
RUN apt-get update
#Install git
RUN apt-get install -y git
WORKDIR /app
RUN git clone https://gitlab.com/nalin_dullu/SpringBoot-HealthCheck-Pipeline.git


FROM base as jdk
LABEL name=jdk
#Install wget
RUN apt-get install -y wget
# Install Java
RUN cd /usr/lib; mkdir jvm; cd jvm \
    && wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz \
    && tar zxf openjdk-11.0.2_linux-x64_bin.tar.gz \
    && rm -f openjdk-11.0.2_linux-x64_bin.tar.gz 
ENV JAVA_HOME=/usr/lib/jvm/jdk-11.0.2
ENV PATH="$PATH:$JAVA_HOME/bin"


FROM jdk as build
LABEL name=mvnbuild
#maven version
ENV MVN_VER 3.5.4
# get maven version
RUN wget --no-verbose -O /tmp/apache-maven-${MVN_VER}.tar.gz http://archive.apache.org/dist/maven/maven-3/${MVN_VER}/binaries/apache-maven-${MVN_VER}-bin.tar.gz
# install maven
RUN tar xzf /tmp/apache-maven-${MVN_VER}.tar.gz -C /opt/
RUN ln -s /opt/apache-maven-${MVN_VER} /opt/maven
RUN ln -s /opt/maven/bin/mvn /usr/local/bin
RUN rm -f /tmp/apache-maven-${MVN_VER}.tar.gz
ENV MAVEN_HOME /opt/maven
WORKDIR /app
COPY --from=base /app/SpringBoot-HealthCheck-Pipeline /app
RUN mvn -B -f pom.xml dependency:resolve
ARG MVNGOAL="package"
ARG MVNARGS="-DskipTests"
RUN mvn -B ${MVNGOAL} ${MVNARGS}


FROM jdk as runtime
LABEL name=springbootRuntime
WORKDIR /app
COPY --from=build /app/target/Infosys-DevOps-ANZ-0.0.1-SNAPSHOT.jar /app
EXPOSE 8080
CMD ["java -jar Infosys-DevOps-ANZ-0.0.1-SNAPSHOT"]




